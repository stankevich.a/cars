<?php

namespace App\Http\Filters;

class CarFilter
{
    protected static $sortFields = ['owner_name', 'national_code', 'color', 'year', 'vin_code'];

    /**
     * @param $query
     * @param $request
     * @return mixed
     */
    public static function applyFilter($query, $request)
    {
        if ($search = $request->get('search')) {
            $query->where(function ($query) use ($search) {
                $query->where('owner_name', 'LIKE','%'.$search.'%')
                    ->orWhere('vin_code', 'LIKE','%'.$search.'%')
                    ->orWhere('owner_name', 'LIKE','%'.$search.'%');
            });
        }

        if ($model = $request->get('model_id')) {
            $query->where('model_id', $model);
        }

        if ($brand = $request->get('brand_id')) {
            $query->whereHas('model', function ($query) use ($brand) {
                $query->where('brand_id', $brand);
            });
        }

        $field = $request->get('sort_filed');

        if (in_array($field, static::$sortFields)) {
            $direction = $request->get('sort_direction');
            $query->orderBy($field, strtolower($direction) === 'desc' ? 'desc' : 'asc');
        }

        return $query;
    }
}
