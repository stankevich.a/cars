<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('car');

        return [
            'owner_name' => 'required|string|max:255',
            'national_code' => 'required|string|max:50',
            'color' => 'required|string|max:255',
            'vin_code' => 'required|string|min:17|max:17|unique:cars,vin_code' . ($id ? ',' . $id : ''),
        ];
    }
}
