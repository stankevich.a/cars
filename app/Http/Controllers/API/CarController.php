<?php

namespace App\Http\Controllers\API;

use App\Console\Commands\VpicImport;
use App\Http\Controllers\Controller;
use App\Http\Filters\CarFilter;
use App\Http\Requests\CarRequest;
use App\Models\Car;
use App\Services\Vpic;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use \Illuminate\Http\Response;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = CarFilter::applyFilter(Car::query(), $request);

        /** @var $result LengthAwarePaginator */
        $result = $query->paginate(config('api.default_page_limit'));

        return new Response([
            'items' => $result->items(),
            'total' => $result->total(),
            'next' => $result->nextPageUrl(),
            'prev' => $result->previousPageUrl(),
            'per_page' => $result->perPage(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CarRequest  $request
     * @return Response
     */
    public function store(CarRequest $request)
    {
        return $this->save(new Car(), $request);
    }

    protected function save(Car $car, CarRequest $request)
    {
        $vinCode = $request->get('vin_code');
        $vpic = resolve(Vpic::class);
        $vinData = $vpic->getDataByVinCode($vinCode);

        if (count($vinData) < 2) {
            return new Response(['success' => false, 'message' => 'Data about current VIN code not found']);
        }

        $car->fill($request->validated());
        $car->model()->associate(array_get($vinData, 'model'));
        $car->year = array_get($vinData, 'year');
        $success = $car->save();

        return new Response(['success' => $success]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return new Response(Car::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CarRequest  $request
     * @param  int  $id
     * @return Response
     */
    public function update(CarRequest $request, $id)
    {
        $car = Car::findOrFail($id);

        return $this->save($car, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $success = (bool) Car::destroy($id);
        return new Response(['success' => $success]);
    }
}
