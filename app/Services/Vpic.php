<?php

namespace App\Services;

use App\Models\Brand;
use App\Models\Model;

class Vpic
{
    const SERVICE_KEY = 'vpic';
    const MODEL_VAR_ID = 28;
    const YEAR_VAR_ID = 29;

    public function getDataByVinCode($code)
    {
        $code = urlencode($code);

        $response = file_get_contents('https://vpic.nhtsa.dot.gov/api/vehicles/decodevin/' . $code . '?format=json');
        $result = json_decode($response, true);
        $result = array_get($result, 'Results', []);

        $data = [];

        foreach($result as $value) {
            if (array_get($value, 'VariableId') == static::MODEL_VAR_ID) {
                $data['model'] = Model::whereExternalSourceId(array_get($value, 'ValueId'))
                    ->whereExternalSourceKey(static::SERVICE_KEY)
                    ->first();
            }

            if (array_get($value, 'VariableId') == static::YEAR_VAR_ID) {
                $data['year'] = array_get($value, 'Value');
            }
        }

        return array_filter($data);
    }
}
