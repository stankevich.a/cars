<?php

namespace App\Console\Commands;

use App\Models\Brand;
use App\Services\Vpic;
use Illuminate\Console\Command;

class VpicImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vpic:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Store new brands and models to local db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $response = file_get_contents('https://vpic.nhtsa.dot.gov/api/vehicles/getallmakes?format=json');
        $brands = array_get(json_decode($response, true), 'Results');

        $brandsCount = 0;

        foreach ($brands as $brand) {
            $this->addBrandWithModels(array_get($brand, 'Make_ID'), array_get($brand, 'Make_Name'));

            if (++$brandsCount % 500 === 0) {
                $this->info("Seen $brandsCount items\n");
            }
        }
    }

    protected function addBrandWithModels($id, $name)
    {
        $brand = Brand::whereExternalSourceId($id)
            ->whereExternalSourceKey(Vpic::SERVICE_KEY)
            ->firstOrCreate([
                'name' => $name,
                'external_source_id' => $id,
                'external_source_key' => Vpic::SERVICE_KEY,
            ]);

        $response = file_get_contents('https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformakeid/' . urlencode($id) . '?format=json');
        $models = array_get(json_decode($response, true), 'Results');

        foreach($models as $model) {
            $id = array_get($model, 'Model_ID');

            if (!$id) {
                continue;
            }

            $brand->models()
                ->where('external_source_id', $id)
                ->where('external_source_key', Vpic::SERVICE_KEY)
                ->firstOrCreate([
                    'name' => $name,
                    'external_source_id' => $id,
                    'external_source_key' => Vpic::SERVICE_KEY,
                ]);
        }
    }
}
