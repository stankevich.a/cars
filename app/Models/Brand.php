<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;


/**
 * \App\Models\Brand
 *
 * @property int $id
 * @property string $name
 * @property string|null $external_source_id
 * @property string|null $external_source_key
 * @method static \Illuminate\Database\Eloquent\Builder|Brand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand query()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereExternalSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereExternalSourceKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereName($value)
 * @mixin \Eloquent
 */
class Brand extends EloquentModel
{
    protected $fillable = ['name', 'external_source_id', 'external_source_key'];

    public $timestamps = false;

    public function models()
    {
        return $this->hasMany(Model::class);
    }
}
