<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

/**
 * \App\Models\Model
 *
 * @property int $id
 * @property string $name
 * @property int $brand_id
 * @property string|null $external_source_id
 * @property string|null $external_source_key
 * @method static \Illuminate\Database\Eloquent\Builder|Model newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model query()
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereExternalSourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereExternalSourceKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Model whereName($value)
 * @mixin \Eloquent
 */
class Model extends EloquentModel
{
    protected $fillable = ['name', 'external_source_id', 'external_source_key'];

    public $timestamps = false;
}
